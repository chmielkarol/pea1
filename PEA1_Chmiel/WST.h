#pragma once
class WST
{
public:
	WST();
	
	WST(unsigned char n, unsigned int* p, unsigned int* w, unsigned int* d);
	
	WST(std::string path);
	
	~WST();

	unsigned int calculate();
	unsigned int calculateBF();

	std::string getOrder();
	std::string getOrderBF();

	bool checkCalc(std::string name);
	bool checkCalcBF(std::string name);

private:
	unsigned int n;
	unsigned int* p;
	unsigned int* w;
	unsigned int* d;
	unsigned int binary;
	unsigned int* wT;
	element* order;
	unsigned int* orderBF;
	unsigned int* orderBF_temp;
	unsigned int minBF;
	
	unsigned int maximum(int value);

	void swap(unsigned int a, unsigned int b);

	void permute(unsigned int start, unsigned int end);

	unsigned int getResult(std::string name);
};

