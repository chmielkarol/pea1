#include "stdafx.h"
#include "WST.h"


WST::WST()
{
}

WST::WST(unsigned char n, unsigned int * p, unsigned int * w, unsigned int * d)
{
	this->n = n;
	this->p = p;
	this->w = w;
	this->d = d;
	binary = 1;
	binary = binary << n; //binary = 2^n
	wT = new unsigned int[binary];
	wT[0] = 0;
	order = new element[binary];
	order[0].value = 0;
	order[0].next = 0;
}

WST::WST(std::string path)
{
	std::ifstream file;
	file.open(path);
	std::string text;
	file >> text;
	file >> n;
	p = new unsigned int[n];
	w = new unsigned int[n];
	d = new unsigned int[n];
	for (unsigned int i = 0; i < n; ++i) {
		file >> p[i] >> w[i] >> d[i];
	}
	file.close();
	binary = 1;
	binary = binary << n; //binary = 2^n
	wT = new unsigned int[binary];
	wT[0] = 0;
	order = new element[binary];
	order[0].value = 0;
	order[0].next = 0;
}

WST::~WST()
{
	delete [] wT;
	delete [] order;
	delete [] orderBF_temp;
}

unsigned int WST::calculate()
{
	for (unsigned int bin = 1; bin < binary; ++bin) { //bin < 2^n; bin <= (2^n-1)
		unsigned int min = UINT_MAX;
		unsigned int pow = 1;
		unsigned int task = 0;
		unsigned int p_sum = 0;
		while (pow <= bin) {
			if (bin & pow) p_sum += p[task];
			pow = pow << 1;
			++task;
		}
		
		pow = 1;
		task = 0;
		while (pow <= bin) {
			if (bin & pow) { //bin zawiera odpowiedni bit reprezentowany przez pow
				unsigned int value = maximum(p_sum - d[task])*w[task] + wT[bin - pow];
				if (value < min) {
					min = value;
					order[bin].value = task+1;
					order[bin].next = bin - pow;
				}
			}
			pow = pow << 1;
			++task;
		}
		wT[bin] = min;
	}
	return wT[binary-1];
}
std::string WST::getOrder()
{
	std::string out = "";
	unsigned int pointer = binary - 1;
	while (order[pointer].value) {
		out.insert(0, ", " + std::to_string(order[pointer].value));
		pointer = order[pointer].next;
	}
	out.erase(0, 2);
	return out;
}

std::string WST::getOrderBF()
{
	std::string out = "";
	for (unsigned int i = 0; i < n; ++i) out += ", " + std::to_string(orderBF[i]  + 1);
	out.erase(0, 2);
	return out;
}

bool WST::checkCalc(std::string name)
{
	unsigned int result = getResult(name);
	if (result != wT[binary - 1]) return false;
	unsigned int pointer = binary - 1;
	unsigned int value = 0;
	unsigned int p_sum = 0;
	unsigned int task;
	for (unsigned int i = 0; i < n; ++i) p_sum += p[i];
	while ((task = order[pointer].value - 1) != -1) {
		value += maximum(p_sum - d[task])*w[task];
		p_sum -= p[task];
		pointer = order[pointer].next;
	}
	return value == result;
}

bool WST::checkCalcBF(std::string name)
{
	unsigned int result = getResult(name);
	if (result != minBF) return false;
	unsigned int value = 0;
	unsigned int p_sum = 0;
	for (unsigned int i = 0; i < n; ++i) {
		unsigned int task = orderBF[i];
		p_sum += p[task];
		value += maximum(p_sum - d[task])*w[task];
	}
	return value == result;
}

unsigned int WST::maximum(int value)
{
	if (value > 0) return value;
	return 0;
}

void WST::swap(unsigned int a, unsigned int b)
{
	unsigned int temp = orderBF_temp[a];
	orderBF_temp[a] = orderBF_temp[b];
	orderBF_temp[b] = temp;
}

void WST::permute(unsigned int start, unsigned int end)
{
	if (start != end) {
		for (unsigned int i = start; i <= end; ++i) {
			swap(start, i);
			permute(start + 1, end);
			swap(start, i);
		}
	}
	else
	{
		unsigned int value = 0;
		unsigned int p_sum = 0;
		for (unsigned int i = 0; i < n; ++i) {
			unsigned int task = orderBF_temp[i];
			p_sum += p[task];
			value += maximum(p_sum - d[task])*w[task];
		}
		if (value < minBF) {
			minBF = value;
			for (unsigned int i = 0; i < n; ++i) orderBF[i] = orderBF_temp[i];
		}
	}
}

unsigned int WST::getResult(std::string name)
{
	std::ifstream file;
	file.open("opt.txt");
	std::string text;
	unsigned int value;
	do {
		file >> text;
	} while (text.compare(name));
	getline(file, text);
	getline(file, text);
	file >> value;
	file.close();
	return value;
}

unsigned int WST::calculateBF()
{
	minBF = UINT_MAX;
	orderBF = new unsigned int[n];
	orderBF_temp = new unsigned int[n];
	for (unsigned int i = 0; i < n; ++i) orderBF_temp[i] = i;
	permute(0, n - 1);
	return minBF;
}
