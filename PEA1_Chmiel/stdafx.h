#pragma once

#include "targetver.h"

#include <iostream>
#include <string>
#include <windows.h>
#include "psapi.h"
#include <fstream>

struct element {
	unsigned int value;
	unsigned int next;
};