// PEA1.cpp: Definiuje punkt wejścia dla aplikacji konsolowej.
//
#include "stdafx.h"
#include "WST.h"
#include "constants.h"

//Procedura startujaca stoper
LARGE_INTEGER startTimer()
{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start;
}

//Procedura zatrzymujaca stoper
LARGE_INTEGER endTimer()
{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop;
}

void measure_time() {
	//zmienne pomiaru pamieci
	PROCESS_MEMORY_COUNTERS pmc;

	srand(SEED);
	//Zmienne stopera
	LARGE_INTEGER performanceCountStart, performanceCountEnd, frequency;
	//Inicjalizacja stopera
	QueryPerformanceFrequency(&frequency);

	WST* task;
	unsigned int *p, *w, *d;
	unsigned int p_sum;

	std::ofstream file, file2;
	file.open("results.txt", std::ios::out);
	file2.open("results_memory.txt", std::ios::out);

	for (unsigned int n = N_MIN; n <= N_MAX; ++n) {
		//Zmienne czasu
		double counter1 = 0;
		double counter2 = 0;

		p = new unsigned int[n];
		w = new unsigned int[n];
		d = new unsigned int[n];
		p_sum = 0;

		for (unsigned int i = 0; i < n; ++i) {
			p_sum += (p[i] = (rand() % 100) + 1);
			w[i] = (rand() % 10) + 1;
		}
		for (unsigned int i = 0; i < n; ++i) d[i] = (unsigned int)((((rand() % 100) + 20) / 100.0)*p_sum);

		task = new WST(n,p,w,d);

		for (unsigned int i = 0; i < REPEAT; ++i) {
			performanceCountStart = startTimer();
				task->calculate();
			performanceCountEnd = endTimer();
			counter1 += (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / frequency.QuadPart;
		}
		if (n <= N_MAX_BF) {
			for (unsigned int i = 0; i < REPEAT; ++i) {
				performanceCountStart = startTimer();
					task->calculateBF();
				performanceCountEnd = endTimer();
				counter2 += (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / frequency.QuadPart;
			}
		}

		GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
		file2 << n << ' ' << pmc.PeakWorkingSetSize / 1024.0 / 1024.0 << '\n';
		
		delete task;
		delete p;
		delete w;
		delete d;

		counter1 /= REPEAT;
		counter2 /= REPEAT;
		if (n <= N_MAX_BF) {
			std::cout << "ROZMIAR: " << n << " | " << "Prog. dynamiczne: " << counter1 << " | " << "Brute Force: " << counter2 << '\n';
			file << n << " " << counter1 << " " << counter2 << '\n';
		}
		else {
			std::cout << "ROZMIAR: " << n << " | " << "Prog. dynamiczne: " << counter1 << '\n';
			file << n << " " << counter1 << '\n';
		}
	}
	file.close();
	file2.close();
}

//Funkcja wyboru opcji z podanej puli opcji
int getOption(std::string text, int min, int max) {
	int value;
	while (true) {
		std::cin.clear();
		system("cls");
		std::cout << text << std::endl;
		std::cout << "Wybor: ";
		std::cin >> value;
		if (std::cin.good() && value >= min && value <= max) return value;
		std::cout << "Podano nieprawidlowy numer.\n";
		system("pause");
	}
}

//Funkcja pobiera nazwe pliku od uzytkownika
std::string getPath() {
	std::string path;
	while (true) {
		std::cin.clear();
		system("cls");
		std::cout << "Podaj sciezke do pliku: ";
		std::cin >> path;
		if (std::cin.good() && !path.empty()) return path;
		std::cout << "Podano nieprawidlowo sciezke.\n";
		system("pause");
	}
}

//Funkcja pobiera wartosc liczbowa od uzytkownika
int getValue(std::string text) {
	int value;
	while (true) {
		std::cin.clear();
		system("cls");
		std::cout << text << std::endl;
		std::cout << "Wartosc: ";
		std::cin >> value;
		if (std::cin.good()) return value;
		std::cout << "Wpisz wartosc poprawnie.\n";
		system("pause");
	}
}

int main()
{
	std::cout << "------Karol Chmiel 235035-----\n";
	std::cout << "--------PEA - Projekt 1-------\n";
	std::cout << "------------------------------\n";
	std::cout << "---Weighted--Sum--Tardiness---\n";
	std::cout << "------------------------------\n";
	std::cout << "---Programowanie-dynamiczne---\n";
	std::cout << "--------------+---------------\n";
	std::cout << "------Przeglad-zupelny--------\n";

	while (true) {
		system("pause");
		switch (getOption(std::string("Operacje:\n")
			+ "1. Oblicz dla danych z pliku\n"
			+ "2. Pomiar czasu dzialania\n"
			+ "3. Ustawienia\n"
			+ "4. Wyjdz", 1, 4)) {
		case 1:
			try {
				std::string path = getPath();
				WST task = WST(path);
				switch (getOption(std::string("Algorytm:\n")
					+ "1. Programowanie dynamiczne\n"
					+ "2. Przeglad zupelny\n", 1, 2)) {
				case 1:
					std::cout << "Wynik: " << task.calculate() << '\n';
					std::cout << "Kolejnosc: " << task.getOrder() << '\n';
					std::cout << "Poprawnosc obliczen: " << ((task.checkCalc(path.substr(0, path.find('.'))) == true) ? "poprawne" : "niepoprawne") << '\n';
					break;
				case 2:
					std::cout << "Wynik: " << task.calculateBF() << '\n';
					std::cout << "Kolejnosc: " << task.getOrderBF() << '\n';
					std::cout << "Poprawnosc obliczen: " << ((task.checkCalcBF(path.substr(0, path.find('.'))) == true) ? "poprawne" : "niepoprawne") << '\n';
				}
			}
			catch (std::ifstream::failure e) {
				std::cout << "\nWystapil blad (" << e.what() << ")";
			}
			break;
		case 2:
			measure_time();
			break;
		case 3:
			switch (getOption(std::string("Zmien:\n")
				+ "1. Ziarno                                     :=   " + std::to_string(SEED) + '\n'
				+ "2. Min. rozmiar problemu                      :=   " + std::to_string(N_MIN) + '\n'
				+ "3. Max. rozmiar problemu (prog. dynamiczne)   :=   " + std::to_string(N_MAX) + '\n'
				+ "4. Max. rozmiar problemu (brute force)        :=   " + std::to_string(N_MAX_BF) + '\n'
				+ "5. Liczba powtorzen dla rozmiaru problemu     :=   " + std::to_string(REPEAT) + '\n'
				+ "6. WSTECZ", 1, 6)) {
			case 1:
				SEED = getOption(std::string("Podaj nowa wartosc..."), INT_MIN, INT_MAX);
				break;
			case 2:
				N_MIN = getOption(std::string("Podaj nowa wartosc..."), 1, N_MAX);
				break;
			case 3:
				N_MAX = getOption(std::string("Podaj nowa wartosc..."), N_MIN, 31);
				break;
			case 4:
				N_MAX_BF = getOption(std::string("Podaj nowa wartosc..."), N_MIN, N_MAX);
				break;
			case 5:
				REPEAT = getOption(std::string("Podaj nowa wartosc..."), 1, 100);
			}
			break;
		case 4:
			return 0;
		}
	}
}

